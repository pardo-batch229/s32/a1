let http = require("http");

http.createServer(function(req,res){
    // The method GET means that we will be retrieving or reading information.
    if(req.url == '/' && req.method == "GET"){
        res.writeHead(200,{"Content-Type": "text/plain"})
        res.end("Welcome to booking system");
    }
    if(req.url == '/profile' && req.method == "GET"){
        res.writeHead(200,{"Content-Type": "text/plain"})
        res.end("Welcome to your profile");
    }
    if(req.url == '/courses' && req.method == "GET"){
        res.writeHead(200,{"Content-Type": "text/plain"})
        res.end("Here's our courses available");
    }
    // The method POST means that we will be adding or creating information.
    if(req.url == '/addCourse' && req.method == "POST"){
        res.writeHead(200,{"Content-Type": "text/plain"})
        res.end("Add course to our resources");
    }
    // The method PUT.
    if(req.url == '/updateCourse' && req.method == "PUT"){
        res.writeHead(200,{"Content-Type": "text/plain"})
        res.end("Update a course to our resources");
    }
    // The method DELETE.
    if(req.url == '/archiveCourse' && req.method == "DELETE"){
        res.writeHead(200,{"Content-Type": "text/plain"})
        res.end("Archive courses to our resources");
    }
    

}).listen(4000);

console.log("System is now running at localhost 4000!");